package com.java2.project;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JTextField;
import java.awt.Font;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Help {

	JFrame Help;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Help window = new Help();
					window.Help.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Help() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		Help = new JFrame();
		Help.setBounds(100, 100, 692, 386);
		Help.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Help.getContentPane().setLayout(null);
		
		JButton btnNewButton = new JButton("Log In");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SignUp sf = new SignUp();
				sf.LOGIN.setVisible(true);
				Help.dispose();
			}
		});
		btnNewButton.setFont(new Font("Arial", Font.PLAIN, 10));
		btnNewButton.setBackground(new Color(0, 102, 255));
		btnNewButton.setBounds(545, 40, 61, 23);
		Help.getContentPane().add(btnNewButton);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon("C:\\Users\\USER\\Downloads\\153012720_495186651474336_6204204395367098588_n.png"));
		lblNewLabel.setBounds(0, 0, 683, 347);
		Help.getContentPane().add(lblNewLabel);
		
		textField = new JTextField();
		textField.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				if(textField.getText().equals("Describe your issue.")) {
					textField.setText("");
				}
				else {
					textField.selectAll();
				}
			}
			@Override
			public void focusLost(FocusEvent e) {
				if(textField.getText().equals(""))
					textField.setText("Describe your issue.");
			}
		});
		textField.setFont(new Font("Arial", Font.PLAIN, 12));
		textField.setBounds(241, 180, 224, 20);
		Help.getContentPane().add(textField);
		textField.setColumns(10);
	}
}
