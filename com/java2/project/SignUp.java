package com.java2.project;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import java.awt.SystemColor;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.SwingConstants;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.border.LineBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class SignUp {

	JFrame LOGIN;
	private JTextField txtUsername;
	private JPasswordField pwdPassword;
	private JLabel lblLoginMessage = new JLabel("");

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SignUp window = new SignUp();
					window.LOGIN.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public SignUp() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		LOGIN = new JFrame();
		LOGIN.getContentPane().setFont(new Font("Arial", Font.PLAIN, 12));
		LOGIN.getContentPane().setForeground(SystemColor.textHighlight);
		LOGIN.getContentPane().setBackground(Color.WHITE);
		LOGIN.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Email");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 22));
		lblNewLabel.setBounds(94, 249, 71, 24);
		LOGIN.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Password");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 22));
		lblNewLabel_1.setBounds(94, 349, 99, 33);
		LOGIN.getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("G");
		lblNewLabel_2.setForeground(Color.BLUE);
		lblNewLabel_2.setFont(new Font("Times New Roman", Font.PLAIN, 50));
		lblNewLabel_2.setBounds(150, 11, 43, 83);
		LOGIN.getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_2_1 = new JLabel("o");
		lblNewLabel_2_1.setForeground(new Color(255, 51, 51));
		lblNewLabel_2_1.setFont(new Font("Times New Roman", Font.PLAIN, 50));
		lblNewLabel_2_1.setBounds(187, 12, 35, 80);
		LOGIN.getContentPane().add(lblNewLabel_2_1);
		
		JLabel lblNewLabel_2_1_1 = new JLabel("o");
		lblNewLabel_2_1_1.setForeground(new Color(255, 204, 102));
		lblNewLabel_2_1_1.setFont(new Font("Times New Roman", Font.PLAIN, 50));
		lblNewLabel_2_1_1.setBounds(214, 14, 35, 76);
		LOGIN.getContentPane().add(lblNewLabel_2_1_1);
		
		JLabel lblNewLabel_2_1_1_1 = new JLabel("g");
		lblNewLabel_2_1_1_1.setForeground(new Color(0, 102, 255));
		lblNewLabel_2_1_1_1.setFont(new Font("Times New Roman", Font.PLAIN, 50));
		lblNewLabel_2_1_1_1.setBounds(242, 11, 39, 83);
		LOGIN.getContentPane().add(lblNewLabel_2_1_1_1);
		
		JLabel lblNewLabel_2_1_1_1_1 = new JLabel("l");
		lblNewLabel_2_1_1_1_1.setForeground(new Color(0, 204, 0));
		lblNewLabel_2_1_1_1_1.setFont(new Font("Times New Roman", Font.PLAIN, 50));
		lblNewLabel_2_1_1_1_1.setBounds(269, 18, 22, 69);
		LOGIN.getContentPane().add(lblNewLabel_2_1_1_1_1);
		
		JLabel lblNewLabel_2_1_1_1_2 = new JLabel("e");
		lblNewLabel_2_1_1_1_2.setForeground(new Color(255, 51, 0));
		lblNewLabel_2_1_1_1_2.setFont(new Font("Times New Roman", Font.PLAIN, 45));
		lblNewLabel_2_1_1_1_2.setBounds(281, 19, 33, 69);
		LOGIN.getContentPane().add(lblNewLabel_2_1_1_1_2);
		
		JLabel lblNewLabel_3 = new JLabel("Sign in to continue to Gmail");
		lblNewLabel_3.setBounds(153, 198, 161, 40);
		LOGIN.getContentPane().add(lblNewLabel_3);
		
		JLabel lblNewLabel_7 = new JLabel("Forgot password?");
		lblNewLabel_7.setForeground(SystemColor.textHighlight);
		lblNewLabel_7.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNewLabel_7.setBounds(170, 458, 106, 24);
		LOGIN.getContentPane().add(lblNewLabel_7);
		
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.setLayout(null);
		panel.setBackground(Color.WHITE);
		panel.setBounds(94, 273, 285, 65);
		LOGIN.getContentPane().add(panel);
		
		JLabel lblNewLabel_9 = new JLabel("");
		lblNewLabel_9.setIcon(new ImageIcon("C:\\Users\\USER\\Downloads\\download.png"));
		lblNewLabel_9.setBounds(245, 22, 30, 30);
		panel.add(lblNewLabel_9);
		
		txtUsername = new JTextField();
		txtUsername.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				if(txtUsername.getText().equals("Username")) {
					txtUsername.setText("");
				}
				else {
					txtUsername.selectAll();
				}
			}
			@Override
			public void focusLost(FocusEvent e) {
				if(txtUsername.getText().equals(""))
					txtUsername.setText("Username");
			}
		});
		txtUsername.setBounds(10, 11, 225, 41);
		panel.add(txtUsername);
		txtUsername.setText("Username");
		txtUsername.setFont(new Font("Tahoma", Font.PLAIN, 13));
		txtUsername.setColumns(10);
		txtUsername.setBorder(null);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel_1.setLayout(null);
		panel_1.setBackground(Color.WHITE);
		panel_1.setBounds(94, 381, 285, 65);
		LOGIN.getContentPane().add(panel_1);
		
		pwdPassword = new JPasswordField();
		pwdPassword.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				if(pwdPassword.getText().equals("Password")) {
					pwdPassword.setEchoChar('●');
					pwdPassword.setText("");
					}
					else {
						pwdPassword.selectAll();
					}
				}
				@Override
				public void focusLost(FocusEvent e) {
					if(pwdPassword.getText().equals(""))
						pwdPassword.setText("Password");
						pwdPassword.setEchoChar((char)0);
			}
		});
		pwdPassword.setText("Password");
		pwdPassword.setFont(new Font("Tahoma", Font.PLAIN, 13));
		pwdPassword.setEchoChar(' ');
		pwdPassword.setBorder(null);
		pwdPassword.setBounds(10, 11, 225, 41);
		panel_1.add(pwdPassword);
		
		JLabel lblNewLabel_9_2 = new JLabel("");
		lblNewLabel_9_2.setIcon(new ImageIcon("C:\\Users\\USER\\Downloads\\download.jfif"));
		lblNewLabel_9_2.setBounds(245, 22, 30, 30);
		panel_1.add(lblNewLabel_9_2);
		
		JLabel lblNewLabel_9_1 = new JLabel("");
		lblNewLabel_9_1.setIcon(new ImageIcon("C:\\Users\\USER\\Downloads\\download (2).png"));
		lblNewLabel_9_1.setBounds(176, 105, 100, 100);
		LOGIN.getContentPane().add(lblNewLabel_9_1);
		
		JButton btnNewButton = new JButton("LOG IN");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(txtUsername.getText().equals("person@gmail.com") && pwdPassword.getText().equals("person123")) {
					//if correct user input
				Google sf = new Google();
				sf.Google.setVisible(true);
				LOGIN.dispose();
				}
				else if(txtUsername.getText().equals("") || txtUsername.getText().equals("Username") || pwdPassword.getText().equals("") || pwdPassword.getText().equals("Password")) {
					lblLoginMessage.setText("Please input all requirments!");
				}
				else {
					lblLoginMessage.setText("Invalid username or password!");
				}
			}
		});
		btnNewButton.setFont(new Font("Arial", Font.BOLD, 20));
		btnNewButton.setBackground(new Color(0, 102, 255));
		btnNewButton.setBounds(94, 508, 284, 65);
		LOGIN.getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("SIGN UP");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Register sf = new Register();
				sf.Register.setVisible(true);
				LOGIN.dispose();
			}
		});
		btnNewButton_1.setForeground(SystemColor.textHighlight);
		btnNewButton_1.setBorder(null);
		btnNewButton_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnNewButton_1.setBackground(Color.WHITE);
		btnNewButton_1.setBounds(294, 584, 102, 33);
		LOGIN.getContentPane().add(btnNewButton_1);
		
		JButton btnNewButton_1_1 = new JButton("Help?");
		btnNewButton_1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Help sf = new Help();
				sf.Help.setVisible(true);
				LOGIN.dispose();
			}
		});
		btnNewButton_1_1.setForeground(SystemColor.textHighlight);
		btnNewButton_1_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnNewButton_1_1.setBorder(null);
		btnNewButton_1_1.setBackground(Color.WHITE);
		btnNewButton_1_1.setBounds(23, 584, 102, 33);
		LOGIN.getContentPane().add(btnNewButton_1_1);
		
		lblLoginMessage.setBorder(null);
		lblLoginMessage.setFont(new Font("Arial", Font.PLAIN, 12));
		lblLoginMessage.setBounds(94, 476, 285, 33);
		LOGIN.getContentPane().add(lblLoginMessage);
		LOGIN.setBackground(new Color(0, 102, 255));
		LOGIN.setBounds(100, 100, 479, 688);
		LOGIN.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
